#Clear enviroment
rm(list=ls())

#load libs
library(data.table)
library(lightgbm)
#library(ggplot2)
library(tictoc)
library(lubridate)
set.seed(0)

#Fast AUC metric
AUC <-function (actual, predicted) {
  r <- as.numeric(frank(predicted))
  n_pos <- as.numeric(sum(actual == 1))
  n_neg <- as.numeric(length(actual) - n_pos)
  auc <- (sum(r[actual == 1]) - n_pos * (n_pos + 1)/2)/(n_pos *  n_neg)
  auc
}

#Fast AUC metric droping NA first
AUCNA <-function (actual, predicted) {
  ind <- which(!is.na(actual) & !is.na(predicted) )
  actual <- actual[ind]
  predicted <- predicted[ind]
  r <- as.numeric(frank(predicted))
  n_pos <- as.numeric(sum(actual == 1))
  n_neg <- as.numeric(length(actual) - n_pos)
  auc <- (sum(r[actual == 1]) - n_pos * (n_pos + 1)/2)/(n_pos *  n_neg)
  auc
}

#Correlation dropping NA
corNA <- function(y_true, y_pred){
  ind <- which( !is.na(y_true)&!is.na(y_pred) )
  cor( y_true[ind], y_pred[ind]  )  
}
###############################################################
###############################################################
###############################################################



###############################################################
###############################################################
###############################################################
train  <- fread('../input/train_transaction.csv')
test   <- fread('../input/test_transaction.csv')
traini <- fread('../input/train_identity.csv')
testi  <- fread('../input/test_identity.csv')
traini <- rbind( traini, testi); rm(testi) ; gc()

#Convert TimeStamp to DateTime
train[, ts := as_datetime( 1509580800 + TransactionDT )  ]
test [, ts := as_datetime( 1509580800 + TransactionDT )  ]
train[, TransactionDT := NULL ]
test [, TransactionDT := NULL ]

#Set Public and Private Testset
test[,isFraud:=NA]
train[, flag := 0]
test[ , flag := 2]
test[1:(0.20*506691), flag := 1]
table(test$flag)

test <- test[,colnames(train),with=F]
mean( colnames(train) == colnames(test) ); gc()

#Concat train + test
raw <- rbind( train, test )
raw <- raw[, colnames(train),with=F];gc()
rm(train,test);gc()

#Merge train_identity to train_transaction
dim(raw)
raw <- merge( raw, traini,by="TransactionID", all.x=T, sort=F )
dim(raw)
colnames(raw)

#Create some time related features
raw[, V1 := NULL]
raw[, year  := year(ts)  ]
raw[, month := month(ts)  ]
raw[, weeky := week(ts)  ]
raw[, day  := day(ts)  ]
raw[, yday  := yday(ts)  ]
raw[, yday  := ifelse( year==2017, yday, yday+365 )  ]
raw[, wday := wday(ts)  ]
raw[, hour := hour(ts)  ]
raw[, minute := minute(ts)  ]
raw[, hour_of_week := wday*24 + hour ]
raw[, nt := .N , by=c("year","month","day","hour","minute")]

#Get the cents of a transaction
raw[, cents := round( TransactionAmt - floor(TransactionAmt) , digits=3 )  ]
###########

#Read and concatenate Features built using script: submission_777_tony_sggpls.py
RAW <- fread('/home/giba/kaggle/ieeefraud/python/train-777_tony_sggpls.csv')
features <- colnames(RAW)[436:ncol(RAW)]
features <- features[ !features %in% c("year","month","dayofyear") ]
raw <- cbind( raw, RAW[,features,with=F] ); gc()
dim(raw)
rm(RAW); gc()

#Create some groups of cards based in diff and other key features
raw[, ud1 := .GRP, by=c("diff","card1") ]
raw[, ud2 := .GRP, by=c("diff","card1","addr1") ]
raw[, ud3 := .GRP, by=c("diff","card1","addr1","P_emaildomain") ]

#Sum Transaction + V127 to look for chained transactions
raw[, TransactionAmt := round( TransactionAmt, digits=4 )  ]
raw[, f0 := round( TransactionAmt + V127, digits=4 )  ]


#Replace NA with -999
for( i in 3:ncol(raw) ){
  if( is.integer(raw[[i]]) | is.numeric(raw[[i]]) ){
    tmp <- raw[[i]]
    tmp[is.na(tmp)] <- -999
    raw[[i]] <- tmp
  }
}; gc()


#return chains of transactions
custclust <- function( vari ,var1, var2, vardt ){
  vardt <- as.numeric( vardt )
  cl <- rep(NA, length(vari) )
  flag <- rep(0, length(vari) )
  count = 1
  for( i in 1:length(vari) ){
    if( !is.na(var1[i]) ){
      if( var1[i]>0 ){
        ind <- which( var2[1:(i-1)] == var1[i] )
        if(length(ind)>0){
          ind = ind[length(ind)]
          if( var1[ind]>0 ){
            if( flag[ind]==0 ){
              if( vari[i]>=vari[ind] ){
                if( abs(vardt[i]-vardt[ind])<365*86400 ){
                  if( is.na(cl[ind]) ){
                    cl[i]   = count
                    cl[ind] = count
                    count = count+1
                    flag[ind] = 1
                  }else{
                    cl[i]   = cl[ind]
                    flag[ind] = 1
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return( as.integer(cl) )
}

#Return chains of transactions based in ud1, ud2, ud3 
setorderv(raw,c("ts"))
raw[, card := custclust( V96 ,V127, f0, ts ) , by=c("ud1") ]
raw[, card_id1 := .GRP , by=c("ud1","card") ]
raw[, card_id1_N := 1:.N , by=c("card_id1") ]
length(unique( raw$card_id1 ))

setorderv(raw,c("ts"))
raw[, card := NULL ]
raw[, card := custclust( V96 ,V127, f0, ts ) , by=c("ud2") ]
raw[, card_id2 := .GRP , by=c("ud2","card") ]
raw[, card_id2_N := 1:.N , by=c("card_id2") ]
length(unique( raw$card_id2 ))

setorderv(raw,c("ts"))
raw[, card := NULL ]
raw[, card := custclust( V96 ,V127, f0, ts ) , by=c("ud3") ]
raw[, card_id3 := .GRP , by=c("ud3","card") ]
raw[, card_id3_N := 1:.N , by=c("card_id3") ]
length(unique( raw$card_id3 ))

setorderv(raw,c("ts"))
raw[, f0 := round( TransactionAmt + V307, digits=4 )  ]
raw[, card := custclust( V96 ,V307, f0, ts ) , by=c("ud1") ]
raw[, card_id4 := .GRP , by=c("ud1","card") ]
raw[, card_id4_N := 1:.N , by=c("card_id4") ]
length(unique( raw$card_id4 ))



#Create time based folds
raw[, fold := 0 ]
raw[(year==2018)&(month==1), fold := 1 ]
raw[(year==2018)&(month==2), fold := 2 ]
raw[(year==2018)&(month==3), fold := 3 ]
raw[(year==2018)&(month==4), fold := 4 ]
raw[(year==2018)&(month==5), fold := 4 ]
raw[(year==2018)&(month>=6), fold := 5 ]


dt <- raw[,.N, keyby=c("year","weeky")]
dt[, V1 := 0:(.N-1)]
raw[,fold2 := dt[J(raw$year,raw$weeky)]$V1 ]
dt <- raw[, list(mean(is.na(isFraud)),.N), keyby=c("year","month","fold","fold2")]


#Create categorical features based on groups
raw[,grp1 := .GRP, by=c( "ud1","card1","V57","V62","V54","V104","P_emaildomain","DeviceType")]
raw[,grp2 := .GRP, by=c( "ud1","id_02","card1","V62","addr1","V104","M4","DeviceType" )]
raw[,grp3 := .GRP, by=c( "ud1","C14","card1","C7","V94","V104","V124","V61","P_emaildomain_bin" )]
raw[,grp4 := .GRP, by=c( "ud3","card1","V53","V104","email_domain_suffix_comp" )]
raw[,grp5 := .GRP, by=c( "ud1","ProductCD","nulls1","card1","V94","V62","V282" )]
raw[,grp6 := .GRP, by=c( "ud1","ProductCD","V96","V127","card1","V56","R_emaildomain_suffix","R_emaildomain_bin","addr1","M5","email_domain_suffix_comp" )]
raw[,grp7 := .GRP, by=c( "ud1","ProductCD","addr1","P_emaildomain","M4","card1","V104","email_domain_suffix_comp" )]
raw[,grp8 := .GRP, by=c( "ud3","ProductCD","V305","V53","V290","DeviceInfo" )]
raw[,grp9 := .GRP, by=c( "ud1","V305","V65","V53","V290","V35","DeviceInfo" )]
raw[,grp10 := .GRP, by=c( "diff","M3","V241","V88","V248","V253","M5","V301","V244","id_27","V27","V31","M2","V15","V33","V25" )]
raw[,grp11 := .GRP, by=c( "grp10","id_33","V26","V2","V6","V7","V286","V4","V5","V19","V313","V10","V90","M4","V104","V293" )]
raw[,grp12 := .GRP, by=c( "grp11","V82","V75","V12","V292","D7" )]
raw[,grp13 := .GRP, by=c( "grp11","V82","V75","V12","V292","D7","V282" )]
raw[,grp14 := .GRP, by=c( "grp11","V82","V75","V12","V292","D7","V282","C14" )]
raw[,grp15 := .GRP, by=c( "ud2","V305","V290","M5","DeviceInfo" )]
raw[,grp16 := .GRP, by=c( "ud3","V305","V61","V290","DeviceInfo" )]
raw[,grp17 := .GRP, by=c( "card_id1","V305","V65","V56","V54","V87","V104","V36","DeviceInfo" )]


#Create Kmeans features based on C count features
M <- as.matrix( raw[,c(17:30),with=F] )
M[ M == -999 ] <- -1
M <- log(2+M)
k1 <- kmeans( M, centers = 20, iter.max = 100, nstart = 25 )
raw[, kmeans1 := k1$cluster ]

#Create Kmeans features based on D time difference features
M <- as.matrix( raw[,c(31:45),with=F] )
M[ M < 0 ] <- -1
M[ is.na(M) ] <- -1
M <- log(2+M)
k1 <- kmeans( M, centers = 20, iter.max = 100, nstart = 25 )
raw[, kmeans2 := k1$cluster ]


#Add more categorical groups
raw[,grp18 := .GRP, by=c( "ud1","V305","V65","V53","V290","V35","kmeans2" )]
raw[,grp19 := .GRP, by=c( "ud1","card1","V305","V300","V301","V281","addr2","C3","id_22","id_21","id_25","id_24","id_08","id_26","id_07","V57"  )]
raw[,grp20 := .GRP, by=c( "card_id4","V107","V92","V87","V53","V290","V35"  )]
raw[,grp21 := .GRP, by=c( "ud2","kmeans2","V305","V152","V143","V165","V150","V144","V145","id_32","V325","id_35","id_11"  )]

#Crean some counts outliers
raw[ C1>=2000  , C1 := 2000 ]
raw[ C2>=2000  , C2 := 2000 ]
raw[ C4>=1000  , C4 := 1000 ]
raw[ C6>=1000  , C4 := 1000 ]
raw[ C8>=800  , C4 := 800 ]
raw[ C9>=240  , C4 := 240 ]
raw[ C14>=240  , C4 := 240 ]
raw[ C10>=520  , C4 := 520 ]
raw[ C11>=520  , C4 := 520 ]
raw[ C12>=520  , C4 := 520 ]
raw[ C13>=1000  , C4 := 1000 ]

#Add more categorical groups
raw[,grp22 := .GRP, by=c( "ud1_P","kmeans1")]
raw[,grp23 := .GRP, by=c( "ud1_P","ProductCD")]
raw[,grp24 := .GRP, by=c( "ud1_P","C1")]
raw[,grp25 := .GRP, by=c( "ud1_P","C2")]
raw[,grp26 := .GRP, by=c( "ud1_P","C3")]
raw[,grp27 := .GRP, by=c( "ud1_P","C4")]
raw[,grp28 := .GRP, by=c( "ud1_P","C5")]
raw[,grp29 := .GRP, by=c( "ud1_P","C6")]
raw[,grp30 := .GRP, by=c( "ud1_P","C7")]
raw[,grp31 := .GRP, by=c( "ud1_P","C8")]
raw[,grp32 := .GRP, by=c( "ud1_P","C9")]
raw[,grp33 := .GRP, by=c( "ud1_P","C10")]
raw[,grp34 := .GRP, by=c( "ud1_P","C11")]
raw[,grp35 := .GRP, by=c( "ud1_P","C12")]
raw[,grp36 := .GRP, by=c( "ud1_P","C13")]
raw[,grp37 := .GRP, by=c( "ud1_P","C14")]
raw[,grp38 := .GRP, by=c( "ud1_P","D2")]
raw[,grp39 := .GRP, by=c( "ud1_P","D3")]
raw[,grp40 := .GRP, by=c( "ud1_P","D4")]
raw[,grp41 := .GRP, by=c( "ud1_P","D5")]
raw[,grp42 := .GRP, by=c( "ud1_P","D6")]
raw[,grp43 := .GRP, by=c( "ud1_P","D7")]
raw[,grp44 := .GRP, by=c( "ud1_P","D8")]
raw[,grp45 := .GRP, by=c( "ud1_P","D9")]
raw[,grp46 := .GRP, by=c( "ud1_P","D10")]
raw[,grp47 := .GRP, by=c( "ud1_P","D11")]
raw[,grp48 := .GRP, by=c( "ud1_P","D12")]
raw[,grp49 := .GRP, by=c( "ud1_P","D13")]
raw[,grp50 := .GRP, by=c( "ud1_P","D14")]
raw[,grp51 := .GRP, by=c( "ud1_P","D15")]
raw[,grp61 := .GRP, by=c("ud1","M6","id_12","V305","M1","M2","M3","V122","id_27","id_22","id_23","id_21","id_07","C3","V286","V98","V284","M7","M8","M9","V281", "V300", "V53", "addr2","id_34","V301","id_32","V54","id_14","id_30","V61","V58","V59","V60","V62","V56","V55","V104","id_38","V284" )]
raw[,grp62 := .GRP, by=c("grp61","id_06","id_17","V123","card3","dist2","V124","V52","card6","id_28","id_20","card4","V277","card5","M4","M5","P_emaildomain","card2","card1" )]
raw[,grp63 := .GRP, by=c("ud1","card1","addr1","addr2","id_22","V57","V58","id_27","V59","V281","V55","id_12","V56","DeviceType","id_18","R_emaildomain","id_37","id_11")]
raw[,grp64 := .GRP, by=c("grp63","C7","id_19","id_06","V286","V61","V53","V123","V125","M4","V52","P_emaildomain","V104","C12","DeviceInfo")]
raw[,grp65 := .GRP, by=c("ud1","id_02","addr2","M5","M3","C7","D7","V122","M2","V286","V123","V104","V94","V304","V303","V300","V55")]
raw[,grp66 := .GRP, by=c("grp65","V53","V56","V301","V54","V281","M4","V61","V62","V52","card6","card4","M7","V124","C12","M9")]
raw[,grp67 := .GRP, by=c("ud1","card1","P_emaildomain","V305","addr2","V300","V57","V58","V59","id_14","id_12","V55","V281","V56","C7","id_11","id_37","id_01")]
raw[,grp68 := .GRP, by=c("grp67","DeviceType","id_18","id_06","V61","V53","addr1","C12","id_03","M4","V104")]
raw[,grp69 := .GRP, by=c( "ud1","M2","M3","M6","M7","M8","M4","M5","D7","D13","D9","D6","D14","D8","C7","C4","C8","C12","C5","C11","C9","C14","C1","C6","C2","D5","D4","D3","M9" ) ]
raw[,grp70 := .GRP, by=c( "ud1","card1","addr1","id_24","id_27","DeviceType","id_36","id_37","id_32","id_15","id_16","id_11","id_12","id_18","id_13","id_06","id_19","id_20","id_38" ) ]
raw[,grp71 := .GRP, by=c( "ud1","card1","C3","addr2","C7","card2","ProductCD","R_emaildomain","D12","D9","C12","addr1","P_emaildomain","D6","C5","C11","TransactionAmt","D15" ) ]
raw[,grp72 := .GRP, by=c( "ud1","card1","V305","V114","V300","V281","V123","V68","V151","V27","V15","V57","V58","V63","V59","V64","V21","V66","V67","V25", "V23","V55","V56" ,"V109" ,"V197","V169","V143","V226","V87","V61","V19" ) ]
raw[,grp73 := .GRP, by=c( "grp72","V69","V242","V90","V53","M5","V75","V12","M6","V282","V248","V303","M4" ) ]


#Create count features 
raw[, catN1 := .N , by="ud1_P" ]
raw[, catN2 := .N , by="ud_D1_ud_D2_" ]
raw[, catN3 := .N , by="ud_D1_ud_D2__P" ]
raw[, catN4 := .N , by="ud_D1_ud_D2__Amt" ]
raw[, catN5 := .N , by="ud_D1_ud_D2__P_Amt" ]
raw[, catN6 := .N , by="ud_D1_ud_D10_" ]
raw[, catN7 := .N , by="ud_D1_ud_D10__P" ]
raw[, catN8 := .N , by="ud_D1_ud_D10__Amt" ]
raw[, catN9 := .N , by="ud_D1_ud_D10__P_Amt" ]
raw[, catN10 := .N , by="ud_D1_ud_D15_" ]
raw[, catN11 := .N , by="ud_D1_ud_D15__P" ]
raw[, catN12 := .N , by="ud_D1_ud_D15__Amt" ]
raw[, catN13 := .N , by="ud_D1_ud_D15__P_Amt" ]
raw[, catN14 := .N , by="ud_D2_ud_D10_" ]
raw[, catN15 := .N , by="ud_D2_ud_D10__P" ]
raw[, catN16 := .N , by="ud_D2_ud_D10__Amt" ]
raw[, catN17 := .N , by="ud_D2_ud_D10__P_Amt" ]
raw[, catN18 := .N , by="ud_D2_ud_D15_" ]
raw[, catN19 := .N , by="ud_D2_ud_D15__P" ]
raw[, catN20 := .N , by="ud_D2_ud_D15__Amt" ]
raw[, catN21 := .N , by="ud_D2_ud_D15__P_Amt" ]
raw[, catN22 := .N , by="ud_D10_ud_D15_" ]
raw[, catN23 := .N , by="ud_D10_ud_D15__P" ]
raw[, catN24 := .N , by="ud_D10_ud_D15__Amt" ]
raw[, catN25 := .N , by="ud_D10_ud_D15__P_Amt" ]


#Create some features based in total sum of number of transactions and total sum of transaction amt 
raw[,my_count:= V294+V298+V285 ]
raw[,my_diff := V307-V310-V317-V320 ]
raw[,my_count2:= V95+V96+V97+V98+V99+V100+V101+V102+V103 ]


#Refactor folding schema
raw[, fold := 0 ]
raw[(year==2018)&(month==1), fold := 1 ]
raw[(year==2018)&(month==2), fold := 2 ]
raw[(year==2018)&(month==3), fold := 3 ]
raw[(year==2018)&(month==4), fold := 4 ]
raw[(year==2018)&(month==5), fold := 4 ]
raw[(year==2018)&(month>=6), fold := 5 ]

raw[weeky==14, fold2 := 24 ]
raw[weeky==15, fold2 := 25 ]
raw[weeky==16, fold2 := 26 ]
raw[weeky==17, fold2 := 27 ]
raw[weeky==18, fold2 := 28 ]
raw[fold==5, fold2 := 29 ]


raw[,list(.N,mean(is.na(isFraud))),keyby=c("year","weeky","fold","fold2")]


FOLDS <- list()
for( i in 1:max(raw$fold2) ){
  px <- which( (raw$fold2 < i)  )
  py <- which( (raw$fold2 >=i)  )
  FOLDS[[i]] <- list(px,py)
}

#Build Target Encoded features for categorical features
feat <- c("")
for( f in c(3:434,444:513,626:706,732:734) ){
  feat <- colnames(raw)[f]
  featname <- paste0( feat,"_te", collapse="X" )
  ypred <- rep( NA_real_, nrow(raw) )
  i=1
  for( i in 1:max(raw$fold2) ){
    px <- FOLDS[[i]][[1]]
    py <- FOLDS[[i]][[2]]
    dt <- raw[px, mean(isFraud,na.rm=T), keyby=feat ]
    ypred[py] <- dt[J(raw[py,feat,with=F])]$V1
  }
  px <- which( (raw$fold2 > 0) & (raw$fold <=4) )
  py <- which( (raw$fold2 ==0) )
  dt <- raw[px, mean(isFraud,na.rm=T), keyby=feat ]
  ypred[py] <- dt[J(raw[py,feat,with=F])]$V1
  
  raw[, c(featname) := list(ypred) ]
  py <- which( raw$fold==4)
  scbase <- AUCNA(raw$isFraud[py], ypred[py] )
  print( paste( featname, scbase, mean(!is.na(ypred))  ) )
}


feats <- colnames(raw)
feats <- gsub(" ", "_", feats)
colnames(raw) <- feats


#Features to drop, raw Categorical 
todrop <- c("ud1_P",
            "ud_D1_ud_D2_"  ,"ud_D1_ud_D2__P","ud_D1_ud_D2__Amt","ud_D1_ud_D2__P_Amt",
            "ud_D1_ud_D10_","ud_D1_ud_D10__P","ud_D1_ud_D10__Amt","ud_D1_ud_D10__P_Amt",
            "ud_D1_ud_D15_","ud_D1_ud_D15__P","ud_D1_ud_D15__Amt","ud_D1_ud_D15__P_Amt",
            "ud_D2_ud_D10_","ud_D2_ud_D10__P","ud_D2_ud_D10__Amt","ud_D2_ud_D10__P_Amt",
            "ud_D2_ud_D15_","ud_D2_ud_D15__P","ud_D2_ud_D15__Amt","ud_D2_ud_D15__P_Amt",
            "ud_D10_ud_D15_","ud_D10_ud_D15__P","ud_D10_ud_D15__Amt","ud_D10_ud_D15__P_Amt",
            "grp1","grp2","grp3","grp4","grp5","grp6","grp7","grp8","grp9","grp10",
            "grp11","grp12","grp13","grp14","grp15","grp16","grp17","grp18","grp19","grp20",
            "grp21","grp22","grp23","grp24","grp25","grp26","grp27","grp28","grp29","grp30",
            "grp31","grp32","grp33","grp34","grp35","grp36","grp37","grp38","grp39","grp40",
            "grp41","grp42","grp43","grp44","grp45","grp46","grp47","grp48","grp49","grp50",
            "grp51",
            "grp61","grp62","grp63","grp64","grp65","grp66","grp67","grp68","grp69","grp70",
            "grp71","grp72","grp73",
            "TransactionDT_converted"
)
features <- colnames(raw)
features <- features[ !features %in% todrop ]
raw <- raw[,features,with=F]; gc()
dim(raw)
###################################################################

#Transform text features to integer
for(i in 3:ncol(raw) ){
  if( is.character(raw[[i]]) )
    raw[[i]] <- as.integer(factor( raw[[i]] ))
}

raw[,list(.N,sum(is.na(isFraud))),keyby=c("year","month","fold","fold2")]


#Split in train and test
train <- raw[ !is.na(isFraud) ]
test  <- raw[  is.na(isFraud) ]
dim(train)
dim(test)


#The first step of training consist of running a LGB model just to calculate Feature Importance and select top features
params <- list(objective = "binary", 
               seed=1,
               boost="gbdt",
               boost_from_average=T,
               num_threads=14,
               learning_rate = 0.005,
               num_leaves = 127,
               feature_fraction = 0.20,
               bagging_freq = 1,
               bagging_fraction = 0.95,
               verbosity = 1)


px <- seq(1,nrow(train),8)
py <- 500001:nrow(train)
length(px); length(py)
feats <- colnames(train)
feats <- feats[ !feats %in% c("TransactionID","isFraud","TransactionDT","month","year","flag","ts","tsn","tsday","year","ypred","weeky","yday","minute","ts","tsday","fold","fold2","fold3",
                              "ud1","ud2","ud3","card_id","card_id2","card_id3","ts_te",
                              "ts_te","flag_te","fold_te","fold2_te","TransactionDT_converted_te","nt_te",
                              "V41_te","V107_te",
                              "card_id","ypred0","ypred1","ypred2","ypred3","ypred4"
) ]
xtrain <- lgb.Dataset(data = as.matrix(train[px,feats,with=F]), label=(train$isFraud[px]), free_raw_data=F  ) ; invisible( gc() )
xvalid <- lgb.Dataset(data = as.matrix(train[py,feats,with=F]), label=(train$isFraud[py]), free_raw_data=F  ) ; invisible( gc() )
tic()
m_gbm <- lgb.train(params, xtrain, 99, valids=list(valid=xvalid), eval_freq=20, early_stopping_rounds = 100, verbose=1 )
imp <- lgb.importance(model = m_gbm )
print(head(imp,50))

#Select only top importance features to run the final model
feats <- imp$Feature[ imp$Gain > 0.00005 ]
length(feats)


#Run top features using whole train set
px <- which(train$fold <= 4)
py <- which(train$fold == 4)
length(px); length(py)
xtrain <- lgb.Dataset(data = as.matrix(train[px,feats,with=F]), label=(train$isFraud[px]), free_raw_data=F  ) ; invisible( gc() )
xvalid <- lgb.Dataset(data = as.matrix(train[py,feats,with=F]), label=(train$isFraud[py]), free_raw_data=F  ) ; invisible( gc() )

tic()
m_gbm1  <- lgb.train(params, xtrain, as.integer(1921*1.05), verbose=1 )

params$seed <- 2
m_gbm2 <- lgb.train(params, xtrain, as.integer(1921*1.05), verbose=1 )
toc()

ypred <- ( predict(m_gbm1, as.matrix(test[,feats,with=F]) ) + predict(m_gbm2, as.matrix(test[,feats,with=F]) ) ) / 2
test[, ypred0 := ypred  ]
# [1941]:	valid's auc:0.961856 
# [1961]:	valid's auc:0.961833 
# [1981]:	valid's auc:0.961823 
# [2001]:	valid's auc:0.961826 
# [2021]:	valid's auc:0.961828 
# 250.776 sec elapsed
# [1] 0.9618834



#Run again feature importance model using different HHyperparameters and threshold to select top features
params <- list(objective = "binary", 
               seed=2,
               boost="gbdt",
               boost_from_average=T,
               num_threads=14,
               learning_rate = 0.005,
               num_leaves = 127,
               feature_fraction = 0.15,
               bagging_freq = 1,
               bagging_fraction = 0.95,
               verbosity = 1)

px <- seq(1,500000,8)
py <- 500001:nrow(train)
length(px); length(py)
feats <- colnames(train)
feats <- feats[ !feats %in% c("TransactionID","isFraud","TransactionDT","month","year","flag","ts","tsn","tsday","year","ypred","weeky","yday","minute","ts","tsday","fold","fold2","fold3",
                              "ud1","ud2","ud3","card_id","card_id2","card_id3","ts_te",
                              "ts_te","flag_te","fold_te","fold2_te","TransactionDT_converted_te","nt_te",
                              "V41_te","V107_te",
                              "card_id","ypred0","ypred1","ypred2","ypred3","ypred4"
) ]
xtrain <- lgb.Dataset(data = as.matrix(train[px,feats,with=F]), label=(train$isFraud[px]), free_raw_data=F  ) ; invisible( gc() )
xvalid <- lgb.Dataset(data = as.matrix(train[py,feats,with=F]), label=(train$isFraud[py]), free_raw_data=F  ) ; invisible( gc() )
tic()
m_gbm <- lgb.train(params, xtrain, 99, valids=list(valid=xvalid), eval_freq=20, early_stopping_rounds = 100, verbose=1 )
imp <- lgb.importance(model = m_gbm )
print(head(imp,50))

#Select only top importance features to run the final model
feats <- imp$Feature[ imp$Gain > 0.00004 ]
length(feats)


#Run top features using whole train set
px <- which(train$fold <= 4)
py <- which(train$fold == 4)
length(px); length(py)
xtrain <- lgb.Dataset(data = as.matrix(train[px,feats,with=F]), label=(train$isFraud[px]), free_raw_data=F  ) ; invisible( gc() )
xvalid <- lgb.Dataset(data = as.matrix(train[py,feats,with=F]), label=(train$isFraud[py]), free_raw_data=F  ) ; invisible( gc() )

tic()
m_gbm1  <- lgb.train(params, xtrain, as.integer(2361*1.05), verbose=1 )

params$seed <- 2
feats <- imp$Feature[ imp$Gain > 0.00005 ]
xtrain <- lgb.Dataset(data = as.matrix(train[px,feats,with=F]), label=(train$isFraud[px]), free_raw_data=F  ) ; invisible( gc() )

m_gbm2 <- lgb.train(params, xtrain, as.integer(2361*1.05), verbose=1 )
toc()

ypred <- ( predict(m_gbm1, as.matrix(test[,feats,with=F]) ) + predict(m_gbm2, as.matrix(test[,feats,with=F]) ) ) / 2
test[, ypred1 := ypred  ]
# [2361]:	valid's auc:0.961945 
# [2381]:	valid's auc:0.961946 
# [2401]:	valid's auc:0.96192 
# [2421]:	valid's auc:0.961944 
# [2441]:	valid's auc:0.961938 
# [2461]:	valid's auc:0.961944 
# 243.45 sec elapsed
# [1] 0.9619466

#Blend run 1 and run 2 predictions
pred <- frank( test$ypred0 ) + frank( test$ypred1 )
pred <- pred / max(pred)
summary(pred)


#Create Submission file
sub <- test[,c("TransactionID"),with=F]
sub[, isFraud := pred ]
sub[['TransactionID']] <- as.integer(sub[['TransactionID']])
head(sub)
summary( pred )
fwrite(sub, "new5_1_final.csv")
