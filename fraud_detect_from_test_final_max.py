#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np  
import pandas as pd  
pd.set_option('display.max_columns',None)
from math import floor

import warnings
warnings.filterwarnings('ignore')


# In[43]:


sub  = pd.read_csv( "new5_1_final.csv" )
sub2 = pd.read_csv( "submission_777_tony_sggpls.csv" )
sub3 = pd.read_csv( "submission_xgb_lgb.csv" )
sub['isFraud'] = sub['isFraud'].rank()+sub2['isFraud'].rank()+sub3['isFraud'].rank()
sub['isFraud'] = sub['isFraud'] / sub['isFraud'].max()

final_submission_filename = 'final_submission.csv'




# In[2]:


train = pd.read_csv('../input/train_transaction.csv')
test = pd.read_csv('../input/test_transaction.csv')


# In[3]:


train['Days'] = train['TransactionDT'] / 3600 / 24
train['Days'] = train['Days'].apply(lambda x: floor(x))


# In[4]:


test['Days'] = test['TransactionDT'] / 3600 / 24
test['Days'] = test['Days'].apply(lambda x: floor(x))


# In[5]:


def corret_card_id(x): 
    x=x.replace('.0','')
    x=x.replace('-999','nan')
    return x


# In[6]:


def create_dict(init_dict, list_keys, list_values): 
    new_dict = init_dict
    
    i=0
    while i<len(list_keys):
        if new_dict.get(list_keys[i],None) is None:
            new_dict[list_keys[i]] = [list_values[i]]
        else:
            new_dict[list_keys[i]] += [list_values[i]]     
        i+=1  
    
    return new_dict


# In[7]:


def create_D_list_round(test_dt, list_dt, list_D):
    D_list = list(np.array([round(((test_dt-kkk) / 3600) / 24, 0) for kkk in list_dt])+np.array([qqq for qqq in list_D]))
    return D_list


# In[8]:


def create_D_list_days(test_dt, list_dt, list_D):
    D_list = list(np.array([test_dt-kkk for kkk in list_dt])+np.array([qqq for qqq in list_D]))
    return D_list


# In[9]:


def create_fraud_df(train_df, test_df, feature,feature_add): 
    # feature - какое-то из D (D1, D2, D10, D15)
    # feature_add - доппараметры при идентификации (addr2, P_emaildomain, TransactionAmt)
    
    # Формируем идентификацию для трейна
    train_df['card_id'] = train_df['card1'].astype(str)+'_*_'
    cards_cols= ['card2', 'card3','card4','card5', 'card6','addr1',feature_add]
    for card in cards_cols:
        train_df['card_id']+= train_df[card].astype(str)+'_*_'
        
    train_df['card_id']=train_df['card_id'].apply(corret_card_id) 
    
    
    # Оставляем в трейне непустые значения по рассматриваемому D
    train_df[feature] = train_df[feature].fillna(-1)
    train_df = train_df[train_df[feature]!=-1]
    
    
    # Разбиваем трейн на фродовую и нефродовую части
    train_fraud = train_df[train_df['isFraud']==1]
    train_notfraud = train_df[train_df['isFraud']==0]
    
    
    
    # Формируем идентификацию для теста
    test_df['card_id'] = test_df['card1'].astype(str)+'_*_'
    cards_cols= ['card2', 'card3','card4','card5', 'card6','addr1',feature_add]
    for card in cards_cols: 
        test_df['card_id']+= test_df[card].astype(str)+'_*_'
        
    test_df['card_id']=test_df['card_id'].apply(corret_card_id)
    
    
    # Оставляем в тесте непустые значения по рассматриваемому D
    test_df[feature] = test_df[feature].fillna(-1)
    test_df = test_df[test_df[feature]!=-1]
    
    
    # Оставляем в тесте только те идентификации, у которых встречается фрод в трейне
    test_df = pd.merge(test_df, train_fraud[['card_id','isFraud']].drop_duplicates(), on='card_id', how='left')
    test_df['isFraud'] = test_df['isFraud'].fillna(-1)
    test_df = test_df[test_df['isFraud']!=-1]
    test_df = test_df[['TransactionID','TransactionDT','Days','card_id','isFraud',feature]]
    
    
    # Формируем словарь для трейна с фродовыми транзакциями (ключ - идентификация, значение - TransactionDT)
    dict_train_fraud_dt = {}
    list_train_fraud_dt = list(train_fraud['TransactionDT'])
    list_train_fraud_cardid = list(train_fraud['card_id'])

    dict_train_fraud_dt = create_dict(dict_train_fraud_dt, list_train_fraud_cardid, list_train_fraud_dt)
        
        
    # Формируем словарь для трейна с нефродовыми транзакциями (ключ - идентификация, значение - TransactionDT) 
    dict_train_notfraud_dt = {}
    list_train_notfraud_dt = list(train_notfraud['TransactionDT'])
    list_train_notfraud_cardid = list(train_notfraud['card_id'])

    dict_train_notfraud_dt = create_dict(dict_train_notfraud_dt, list_train_notfraud_cardid, list_train_notfraud_dt)

        
    # Формируем словарь для трейна с фродовыми транзакциями (ключ - идентификация, значение - Days)
    dict_train_fraud_days = {}
    list_train_fraud_days = list(train_fraud['Days'])
    list_train_fraud_cardid = list(train_fraud['card_id'])

    dict_train_fraud_days = create_dict(dict_train_fraud_days, list_train_fraud_cardid, list_train_fraud_days)
        
        
    # Формируем словарь для трейна с нефродовыми транзакциями (ключ - идентификация, значение - Days) 
    dict_train_notfraud_days = {}
    list_train_notfraud_days = list(train_notfraud['Days'])
    list_train_notfraud_cardid = list(train_notfraud['card_id'])

    dict_train_notfraud_days = create_dict(dict_train_notfraud_days, list_train_notfraud_cardid, list_train_notfraud_days)
    
    
    # Формируем словарь для трейна с фродовыми транзакциями (ключ - идентификация, значение - D, по которому считаем)
    dict_train_fraud_D = {}
    list_train_fraud_D = list(train_fraud[feature])
    list_train_fraud_D_cardid = list(train_fraud['card_id'])

    dict_train_fraud_D = create_dict(dict_train_fraud_D, list_train_fraud_D_cardid, list_train_fraud_D)   
    

    # Формируем словарь для трейна с нефродовыми транзакциями (ключ - идентификация, значение - D, по которому считаем)
    dict_train_notfraud_D = {}
    list_train_notfraud_D = list(train_notfraud[feature])
    list_train_notfraud_D_cardid = list(train_notfraud['card_id'])

    dict_train_notfraud_D = create_dict(dict_train_notfraud_D, list_train_notfraud_D_cardid, list_train_notfraud_D)     
    
    
    
    # Формируем список потенциального фрода
    list_selest_test_id = []
    list_test_id = list(test_df['TransactionID'])
    list_test_dt = list(test_df['TransactionDT'])
    list_test_days = list(test_df['Days'])
    list_test_cardid = list(test_df['card_id'])
    list_test_D = list(test_df[feature])

    i=0
    while i<len(list_test_id):

        if feature in ['D1','D2']:
            temp_D_fraud_list = create_D_list_days(list_test_days[i], dict_train_fraud_days[list_test_cardid[i]], dict_train_fraud_D[list_test_cardid[i]])
            try:  
                temp_D_notfraud_list = create_D_list_days(list_test_days[i], dict_train_notfraud_days[list_test_cardid[i]], dict_train_notfraud_D[list_test_cardid[i]])
            except:
                temp_D_notfraud_list = []
      
        elif feature in ['D10','D15']:
            temp_D_fraud_list = create_D_list_round(list_test_dt[i], dict_train_fraud_dt[list_test_cardid[i]], dict_train_fraud_D[list_test_cardid[i]])
            try:  
                temp_D_notfraud_list = create_D_list_round(list_test_dt[i], dict_train_notfraud_dt[list_test_cardid[i]], dict_train_notfraud_D[list_test_cardid[i]])
            except:
                temp_D_notfraud_list = []       
        
        
        
        if list_test_D[i] in temp_D_fraud_list and list_test_D[i] not in temp_D_notfraud_list:
            list_selest_test_id += [list_test_id[i]]
        

        
        i+=1

       
    #Сохраняем список потенциального фрода
    df_selest_test_id = pd.DataFrame({'TransactionID': list_selest_test_id,'counter': list(np.ones(len(list_selest_test_id)))})
    df_selest_test_id.to_csv('./final_fraud_id_files\\fraud_detect_addr1_'+feature_add+'_'+feature+'.csv', index=False)
    
   


# In[ ]:





# In[10]:



# Формируем списки потенциального фрода
for add_feat in ['addr2','TransactionAmt','P_emaildomain']:
    for feat in ['D1','D2','D10','D15']:
        create_fraud_df(train,test,feat,add_feat)


# In[ ]:





# In[11]:


from glob import glob
list_fraud_files = glob('./final_fraud_id_files/'+'*.csv')
fraud_df = pd.DataFrame({'TransactionID': [],'counter': []})


# In[12]:


for k in list_fraud_files:
    temp_fraud_df = pd.read_csv(k)
    
    fraud_df = pd.concat([fraud_df,
                            temp_fraud_df
                           ],axis=0).drop_duplicates()


# In[14]:


i=0
for k in list_fraud_files:
    temp_fraud_df = pd.read_csv(k)
    temp_fraud_df = temp_fraud_df.rename(columns={'counter': 'counter'+str(i)})
    i+=1
    
    fraud_df = fraud_df.merge(temp_fraud_df,on='TransactionID',how='left')
    
fraud_df = fraud_df.fillna(0)


# In[15]:


fraud_df['sum'] = np.zeros(len(fraud_df))
for col in [q for q in fraud_df.columns if q not in ['TransactionID','counter','sum']]:
    fraud_df['sum'] += fraud_df[col] 


# In[16]:


fraud_best = fraud_df[fraud_df['sum']>1]


# In[2]:


# fraud_best.to_csv('final_leak_id.csv',index = False)
#fraud_best = pd.read_csv('final_leak_id.csv')


# In[ ]:





# In[ ]:





# In[44]:


threshold = np.percentile(sub['isFraud'], 98.7)
# threshold was found by validation


# In[ ]:





# In[46]:


sub = sub.merge(fraud_best[['TransactionID','counter','sum']], on='TransactionID',how = 'left')


# In[47]:


sub['counter'] = sub['counter'].fillna(0)
sub['sum'] = sub['sum'].fillna(0)


# In[48]:


sub['counter'] = sub['counter'].apply(lambda x: 1 if x>0 else 0)
sub['isFraud_new'] = sub['isFraud']+sub['counter']


# In[49]:


sub['isFraud_new'] = sub['isFraud_new'].apply(lambda x: threshold if x>=1 else x)
sub['isFraud_new'] = sub['isFraud_new']+sub['sum']*0.0001


# In[57]:


sub['isFraud'] = np.maximum(sub['isFraud'], sub['isFraud_new'])


# In[60]:


sub = sub.drop(['counter','sum', 'isFraud_new'],axis=1)


# In[ ]:





# In[61]:


sub.to_csv(final_submission_filename, index=False)


# In[ ]:




