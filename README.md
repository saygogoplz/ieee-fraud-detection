## Install R
```bash
host$ sudo apt install gcc cmake -y
host$ sudo apt install r-base -y
host$ git clone https://github.com/microsoft/LightGBM.git
host$ R
> install.packages("data.table")
> install.packages("R6")
> install.packages("jsonlite")
> install.packages("tictoc")
> install.packages("lubridate")
> quit()
host$ cd LightGBM/
host$ Rscript build_r.R
host$ cd ..
```

## Fit and make submissions

```bash
host$ ls ../input
sample_submission.csv
test_identity.csv
test_transaction.csv
train_identity.csv
train_transaction.csv

host$ ls
README.md
submission_777_tony_cpmp_shifts_giba_fs-1.py
submission_777_tony_sggpls.py
python3 submission_xgb_lgb.py
TransactionAmt_mean_std_count_diff.npy
fraud_detect_from_test_final_max.ipynb
requirements.txt
host$ pip3 install -r requirements.txt

host$ python3 submission_777_tony_cpmp_shifts_giba_fs-1.py
host$ python3 submission_777_tony_sggpls.py
host$ python3 submission_xgb_lgb.py
host$ Rscript new5_final.r
host$ python3 fraud_detect_from_test_final_max.py

```

## Descriptions

### submission_777_tony_sggpls.py
This is one of our model for blending.
One run-execution (train + test in one pass).

Main idea - catboost perfect works with cat features =)
Just give it to catboost.
We noticed that ds-feattures and TransactionDT has interesting properties for identify one-card-transactions chain.
For refinement this chains we use other features - emails, addrs and so on (look at code pls).

D1 is the main d-feature for chains.
And we use some features from kaggle kernels for working with email-domain and so on.

Other main idea is using different aggregations:
`df[new_col_name] = df[base_col] / df.groupby([group_col])[base_col].transform(op)`

Op is aggregation type : may be std, mean, max, skew and so on (look at code for details).

How it works: if in group std is small, this group is uniform and we can use other statistics for this group (max, mean or similar statistics) as very informative.

And run catboost on gpu.
Whole run needs about 2-3 hours.


### new5_final.R
This script take some features created in submission_777_tony_sggpls.py and add more categorical interactions between other features and Time Difference Features. Also the training algorithm used in this script is LightGBM and before training a feature selection algorithm is performed to select only top features. This script Scores 0.9644 Public LB and 0.9390 Private LB.



### fraud_detect_from_test_final_max.py
This is post-processing after blending.

It don't need train, only one-execution postprocessing of prediction.
We noticed that ds-feattures and TransactionDT has interesting properties for identify one-card-transactions chain.
For refinement this chains we use other features - emails, addrs and so on (look at code pls).
And with this chains we only increase prediction score (use max of postproc and score after blending).

Exection time is about 30-60 minutes.

## Minimum Hardware requirements

RAM: 128GB
VRAM: 11GB (we use xgboost, lgb and catboost in gpu-mode for performance)
Storage: 64GB